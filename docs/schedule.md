---
layout: SpecialPage
---
# Course Syllabus

[[toc]]

## Overview

<!--Both on-campus and OMS student should watch  Udacity course videos. On-campus student should watch video before class. Deliverable due dates apply to both OMS and on-campus student.-->
Students should watch Udacity course videos according to the following schedule. It is **recommended** for students to do lab sessions on the schedule by yourself **as early as possible** since some of homework may cover the lab materials scheduled later than the homework.
For the online video lectures, CS/CSE students should go to Udacity or Canvas to access to the sources.
## Schedule
| Week # | Dates     |  Video lessons                       | Lab                              | Deliverable Due                                                          | 
|--------|-----------|---------------------------------------------------------------------|-------------------------------------|----------------------------------|--------------------------------------------------------------------------| 
| 1      | Jan 18-22  |  [1. Intro to Big Data Analytics], [2. Course Overview]     |                        |                                                                          | 
| 2      | Jan 25-29 |  [3. Predictive Modeling]              |   [Hadoop & HDFS Basics]                               |    HW1 Due (Jan 31)                                                                       | 
| 3      | Feb 1-5 |  [4.MapReduce]& [HBase]                  |   [Hadoop Pig & Hive]                               |                                                           | 
| 4      | Feb 8-12 |  [5.Classification evaluation metrics], [6.Classification ensemble methods] |                             |   HW2 Due (Feb 14)                                                                        | 
| 5      | Feb 15-19  |  [7. Phenotyping], [8. Clustering]                      |  [Scala Basic], [Spark Basic], [Spark SQL]                                |                                                                          | 
| 6      | Feb 22-26 |  [9. Spark]                            |   [Spark Application] & [Spark MLlib]                               |    HW3 Due & Project Group Formation & Project Requirements Release (proposal/draft/final) (Feb 28)                                                                      | 
| 7      | Mar 1-5 |  [10. Medical ontology]                 |  [NLP Lab]                                |                                                                          | 
| 8      | Mar 8-12 |  [11. Graph analysis]                  | [Spark GraphX]                                 |   Project Proposal Due (Mar 14)                                                                        | 
| 9      | Mar 15-19  |  [12. Dimensionality Reduction], [13. Patient similairty], [14. CNN]        |   [Deep Learning Lab]                               |       HW4 Due (Mar 21)                                                                   | 
| 10     | Mar 22-26 |   [15. DNN], [16. RNN]               | |                                                                          | 
| 11     | Mar 29-Apr 2 |   Project Discussion                                    |                                  |                                                                           HW5 Due (Apr 4)
| 12     | Apr 5-9 |  Project Discussion                                                 |                                     |                                   | 
| 13     | Apr 12-16 |  Project Discussion                                    |                                  |                                                Project Draft Due (Apr 18)                          | 
| 14     | Apr 19-23 |  Project Discussion                                    |                                  |                       Final Exam (Apr 25)                                                   | 
| 15     | Apr 26-30 |  Project Discussion                                     |                                  |       Final Project Due (code + presentation + final paper) (May 2)                                                                   | 
| 16     | May 3-6 |   Project Submission                                    |                                  |  | 

<!--update @Dec 2, 2020
| Week # | Dates     |  Video lessons                       | Lab                              | Deliverable Due                                                          | 
|--------|-----------|---------------------------------------------------------------------|-------------------------------------|----------------------------------|--------------------------------------------------------------------------| 
| 1      | Aug 17-21  |  [1. Intro to Big Data Analytics], [2. Course Overview]     |                        |                                                                          | 
| 2      | Aug 24-28 |  [3. Predictive Modeling]              |   [Hadoop & HDFS Basics]                               |    HW1 Due (Aug 30)                                                                       | 
| 3      | Aug 31- Sep 4 |  [4.MapReduce]& [HBase]                  |   [Hadoop Pig & Hive]                               |                                                           | 
| 4      | Sep 7-11 |  [5.Classification evaluation metrics], [6.Classification ensemble methods] |                             |   HW2 Due (Sep 13)                                                                        | 
| 5      | Sep 14-18  |  [7. Phenotyping], [8. Clustering]                      |  [Scala Basic], [Spark Basic], [Spark SQL]                                |                                                                          | 
| 6      | Sep 21-25 |  [9. Spark]                            |   [Spark Application] & [Spark MLlib]                               |    HW3 Due & Project Group Formation & Project Requirements Release (proposal/draft/final) (Sep 27)                                                                      | 
| 7      | Sep 28- Oct 2 |  [10. Medical ontology]                 |  [NLP Lab]                                |                                                                          | 
| 8      | Oct 5-9 |  [11. Graph analysis]                  | [Spark GraphX]                                 |   Project Proposal Due (Oct 11)                                                                        | 
| 9      | Oct 12-16  |  [12. Dimensionality Reduction], [13. Patient similairty], [14. CNN]        |   [Deep Learning Lab]                               |       HW4 Due (Oct 18)                                                                   | 
| 10     | Oct 19-23 |   [15. DNN], [16. RNN]               | |                                                                          | 
| 11     | Oct 26-30 |   Project Discussion                                    |                                  |                                                                           HW5 Due (Nov 1)
| 12     | Nov 2-6 |  Project Discussion                                                 |                                     |                                   | 
| 13     | Nov 9-13 |  Project Discussion                                    |                                  |                                                Project Draft Due (Nov 8)                          | 
| 14     | Nov 16-20 |  Project Discussion                                    |                                  |                                                                          | 
| 15     | Nov 23-27 |  Project Discussion                                     |                                  |           Final Exam (Dec 1)                                                               | 
| 16     | Noc 30-Dec 4 |   Project Submission                                    |                                  | Final Project Due (code + presentation + final paper) (Dec 6) | 
-->
<!-- Updated @ Oct.30, 2019
|Week #|Dates    |In-class lesson                            |Video lessons                                       |Lab                                   |Deliverable Due                                                             |
|:-----|:--------|:------------------------------------------|:---------------------------------------------------|:-------------------------------------|:---------------------------------------------------------------------------|
|1     |1/7/2020 |Intro to the BDH class (Jeff)              |1. Intro to Big Data Analytics                      |                                      |                                                                            |
|1     |1/9/2020 |Predictive modeling (Jimeng)            	 |2. Course Overview                                  |			                             |                                                                            |
|2     |1/14/2020|Lab: Scala Basic (TA)                      |3. Predictive Modeling                              |Scala Basic                           |                                                                            |
|2     |1/16/2020|Lab: Hadoop & HDFS Basic(TA)               |                                                    |Hadoop & HDFS Basic                   |HW1 Due (1/19/2020)                                                         |
|3     |1/21/2020|Project: Prediction in ICU for Mortality/Sepsis (Yanbo)|4.MapReduce& HBase                                 |                                      |                                                                 |
|3     |1/23/2020|Lab: Hadoop Pig & Hive (Su Young)			 |                                                    |Hadoop Pig & Hive         			 |                                                                            |
|4     |1/28/2020|Project: Chest X-Ray/NLP (Siddarth)        |5.Classification evaluation metrics                 |                                      |                                                                            |
|4     |1/30/2020|Project: Drug Discovery (Tianfan)          |6.Classification ensemble methods                   |                     				 |HW2 Due (2/2/2020)                                                          |
|5     |2/4/2020 |Lab: Spark Basic, Spark SQL (Wendi)		 |7. Phenotyping                                      |Spark Basic, Spark SQL                |                                                      					  |
|5     |2/6/2020 |Lab: Spark Application & Spark MLlib (Wendi)|8. Clustering  9. Spark                            |Spark Application & Spark MLlib       |                                                                            |
|6     |2/11/2020|Project: Tensor Factorization (Ari)        |9. Spark                                            |                                      |                                                                            |
|6     |2/13/2020|Project: Sleep Data (Irfan)|               |											          |HW3 Due & Project Group Formation Due & Project Requirements Release (2/16/2020)|
|7     |2/18/2020|Lecture: Tensor Factorization (Ari)        |10. Medical ontology                                |                                      |                                                                            |
|7     |2/20/2020|Lab: NLP (Charity)                         |                                                    |NLP Lab by Charity Hilton             |                                                                            |
|8     |2/25/2020|Joyce Ho (Emory) - The Automation of Evidence Matching and Systematic Reviews Using Web-Based Medical Literature|11. Graph analysis                                  |                                      |                                                                            |
|8     |2/27/2020|Alaa Aljiffry (CHOA) - Big Data in Pediatric Cardiac ICU|                                        |				                         |Project Proposal Due (3/1/2020)                                             |
|9     |3/3/2020 |										     |12. Dimensionality Reduction  13. Patient similarity|                                      |                                                        |
|9     |3/5/2020 |Lecture: Deep Learning (Adversarial Examples for Electronic Health Records - Sungtae)|14. DNN                                     |Deep Learning Lab by Sungtae An       |HW4 Due (3/8/2020)                                                          |
|10    |3/10/2020|Lab: Deep Learning (Sungtae)			     |15. CNN 		                                      |Deep Learning Lab					 |                                                                            |
|10    |3/12/2020|Lab: Deep Learning (Sungtae)			     |16. RNN                                             |                                      |                                                                            |
|11    |3/17/2020|Spring break                               |                                   		          |                                      |                                                                            |
|11    |3/19/2020|Spring break                               |                                                    |                                      |HW5 Due (3/29/2020)                                                         |
|12    |3/24/2020|(cancelled)Diyi Yang (GT) - NLP in Healthcare|                                                    |                                      |                                                                            |
|12    |3/26/2020|(cancelled)Gari Clifford (GT/Emory) - How Big is Big? Pitfalls and Opportunities in ML for Medical Data|                                                    |                                      |                                                                            |
|13    |3/31/2020|(cancelled)Jon Duke - Precision Medicine at Georgia Tech: Introduction to the Health Data Analytics Platform                              |                                                    |                                      |                                                                            |
|13    |4/2/2020 |(cancelled)Omar Inan (GT)					     |                                                    |                                      |Project Draft Due (4/12/2020)                                                |
|14    |4/7/2020 |				                             |                                                    |                                      |                                                                            |
|14    |4/9/2020 |							                 |                                                    |                                      |                                                                            |
|15    |4/14/2020|                							 |                                                    |                                      |                                                                            |
|15    |4/16/2020|							                 |                                                    |Final Exam(4/16/2020 on-campus cancelled)       |Final Exam(4/18-4/20 online cancelled)    			  		  						  |
|16    |4/21/2020|							                 |                                                    |                                      |                                                                            |
|16    |4/23/2020|							                 |                                                    |                                      |Final Project with code, presentation, and the final paper (4/26/2020)      |
-->

<!--update @Oct 30, 2019
| Week # | Dates     |  Video lessons                       | Lab                              | Deliverable Due                                                          | 
|--------|-----------|---------------------------------------------------------------------|-------------------------------------|----------------------------------|--------------------------------------------------------------------------| 
| 1      | Aug 19-23  |  [1. Intro to Big Data Analytics], [2. Course Overview]     |  [Scala Basic]                      |                                                                          | 
| 2      | Aug 26-30 |  [3. Predictive Modeling]              |   [Hadoop & HDFS Basics]                               |    HW1 Due (Sep 1)                                                                       | 
| 3      | Sep 2-6 |  [4.MapReduce]& [HBase]                  |   [Hadoop Pig & Hive]                               |                                                           | 
| 4      | Sep 9-13 |  [5.Classification evaluation metrics], [6.Classification ensemble methods] |                             |   HW2 Due (Sep 15)                                                                        | 
| 5      | Sep 16-20  |  [7. Phenotyping], [8. Clustering]                      |  [Spark Basic], [Spark SQL]                                |                                                                          | 
| 6      | Sep 23-27 |  [9. Spark]                            |   [Spark Application] & [Spark MLlib]                               |    HW3 Due & Project Group Formation & Project Requirements Release (proposal/draft/final) (Sep 29)                                                                      | 
| 7      | Sep 30-4 |  [10. Medical ontology]                 |  [NLP Lab]                                |                                                                          | 
| 8      | Oct 7-11 |  [11. Graph analysis]                  | [Spark GraphX]                                 |   Project Proposal Due (Oct 13)                                                                        | 
| 9      | Oct 14-18  |  [12. Dimensionality Reduction], [13. Patient similairty], [14. DNN]        |   [Deep Learning Lab]                               |       HW4 Due (Oct 20)                                                                   | 
| 10     | Oct 21-25 |   [15. CNN], [16. RNN]               | |                                                                          | 
| 11     | Oct 28- Nov 1 |   Potential Guest Lecture                                    |                                  |                                                                           HW5 Due (Nov 3)
| 12     | Nov 4-8 |  Potential Guest Lecture                                                 |                                     |                                   | 
| 13     | Nov 11-15 |  Potential Guest Lecture                                    |                                  |                                                Project Draft Due (Nov 10)                          | 
| 14     | Nov 18-22 |  Project Discussion                                    |                                  |                                                                          | 
| 15     | Nov 25-29 |  Project Discussion                                     |                                  |           Final Exam (Dec 3)                                                               | 
| 16     | Dec 2-6 |   Project Submission                                    |                                  | Final Project Due (code + presentation + final paper) (Dec 8) | 
-->

<!--
| Week # | Dates     |  Video lessons                       | Lab                              | Deliverable Due                                                          | 
|--------|-----------|---------------------------------------------------------------------|-------------------------------------|----------------------------------|--------------------------------------------------------------------------| 
| 1      | Aug 19-23  |  [1. Intro to Big Data Analytics], [2. Course Overview]     |  [Scala Basic]                      |                                                                          | 
| 2      | Aug 26-30 |  [3. Predictive Modeling]              |   [Hadoop & HDFS Basics]                               |    HW1 Due (Sep 1)                                                                       | 
| 3      | Sep 2-6 |  [4.MapReduce]& [HBase]                  |   [Hadoop Pig & Hive]                               |                                                           | 
| 4      | Sep 9-13 |  [5.Classification evaluation metrics], [6.Classification ensemble methods] |                             |   HW2 Due (Sep 15)                                                                        | 
| 5      | Sep 16-20  |  [7. Phenotyping], [8. Clustering]                      |  [Spark Basic], [Spark SQL]                                |                                                                          | 
| 6      | Sep 23-27 |  [9. Spark]                            |   [Spark Application] & [Spark MLlib]                               |    HW3 Due & Project Group Formation & Project Requirements Release (proposal/draft/final) (Sep 29)                                                                      | 
| 7      | Sep 31-4 |  [10. Medical ontology]                 |  [NLP Lab]                                |                                                                          | 
| 8      | Oct 7-11 |  [11. Graph analysis]                  | [Spark GraphX]                                 |   Project Proposal Due (Oct 13)                                                                        | 
| 9      | Oct 14-18  |  [12. Dimensionality Reduction], [13. Patient similairty], [14. DNN]        |   [Deep Learning Lab]                               |       HW4 Due (Oct 20)                                                                   | 
| 10     | Oct 21-25 |   [15. CNN], [16. RNN]               | |                                                                          | 
| 11     | Oct 28- Nov 1 |   Potential Guest Lecture                                    |                                  |                                                                           HW5 Due (Nov 3)
| 12     | Nov 4-8 |  Potential Guest Lecture                                                 |                                     |                                   | 
| 13     | Nov 11-15 |  Potential Guest Lecture                                    |                                  |                                                Project Draft Due (Nov 10)                          | 
| 14     | Nov 18-22 |  Project Discussion                                    |                                  |                                                                          | 
| 15     | Nov 25-29 |  Project Discussion                                     |                                  |           Final Exam (Dec 3)                                                               | 
| 16     | Dec 2-6 |   Final Exam Week                                    |                                  | Final Project Due (code + presentation + final paper) (Dec 8) | 
-->

<!--
## Schedule
| Week # | Dates     | In-class lesson                                                     | Video lessons                       | Lab                              | Deliverable Due                                                          | 
|--------|-----------|---------------------------------------------------------------------|-------------------------------------|----------------------------------|--------------------------------------------------------------------------| 
| 1      | 1/8/2019  | Intro to the BDH class                                              | [1. Intro to Big Data Analytics](https://classroom.udacity.com/courses/ud758/lessons/6311012028/concepts/63454385810923)      |                                  |                                                                          | 
| 1      | 1/10/2019 | Sunlab's research by Jimeng Sun                                     | [2. Course Overview](https://classroom.udacity.com/courses/ud758/lessons/6311012028/concepts/63454385820923)                  | [Scala Basic](/spark/scala-basic.html)                      |                                                                          | 
| 2      | 1/15/2019 | Deep learning for healthcare by Edward Choi                         | [3. Predictive Modeling](https://classroom.udacity.com/courses/ud758/lessons/5484251492/concepts/63794523170923)              |                                  |                                                                          | 
| 2      | 1/17/2019 | **Guest lecture by Mark Braunstein**                                    |                                     |                                  |  HW1 Due(1/20/2019)                                                                        |
| 3      | 1/22/2019 | Deep learning for healthcare by Edward Choi - Cont.                 | [4.MapReduce](https://classroom.udacity.com/courses/ud758/lessons/6298155413/concepts/63742732970923)& [HBase](/hadoop/hadoop-hbase.html)                  |                                  |                                                           | 
| 3      | 1/24/2019 |                                                                     |                                     | [Hadoop & HDFS Basics](/hadoop/hdfs-basic.html)             |                                                                          | 
| 4      | 1/29/2019 | **Guest lecture by Chunhua Wen (Columbia)**                             | [5.Classification evaluation metrics](https://classroom.udacity.com/courses/ud758/lessons/5505090946/concepts/64260385420923) |                                  |                                                                          | 
| 4      | 1/31/2019  |                                                                     | [6.Classification ensemble methods](https://classroom.udacity.com/courses/ud758/lessons/5615268587/concepts/64019285500923)   | [Hadoop Pig](/hadoop/hadoop-pig.html) & [Hive](/hadoop/hadoop-hive.html)                |   HW2 Due (2/3/2019)                                                                       | 
| 5      | 2/5/2019  | **Guest lecture by Jon Duke**                                           | [7. Phenotyping](https://classroom.udacity.com/courses/ud758/lessons/6363218753/concepts/64354692300923)                      |                                  |                                                                          | 
| 5      | 2/7/2019  |                                                                     | [8. Clustering](https://classroom.udacity.com/courses/ud758/lessons/6343118554/concepts/63741932910923)                       | "[Spark Basic](/spark/spark-basic.html), [Spark SQL](/spark/spark-sql.html) "         |                                                           | 
| 6      | 2/12/2019 | **Guest lecture by Rachel Patzer (Emory)**                              | [9. Spark](https://classroom.udacity.com/courses/ud758/lessons/6376189383/concepts/68610627160923)                            |                                  |                                                                          | 
| 6      | 2/14/2019 |                                                                     |                                     | [Spark Application](/spark/spark-application.html) & [Spark MLlib](/spark/spark-mllib.html)  |  HW3 Due & Project Group Formation & Project Requirements Release (2/17/2019)                                                                        | 
| 7      | 2/19/2019 | Computational phenotyping with tensor factorization by Kimis Perros (I)     | [10. Medical ontology](https://classroom.udacity.com/courses/ud758/lessons/6370309670/concepts/68607648580923)                 |                                  |                                                                          | 
| 7      | 2/21/2019 |  |                                     |  [NLP Lab by Charity Hilton](/nlp/solr.html)      |                                                       | 
| 8      | 2/26/2019 | **Guest lecture by David Page (UW Madison)**                            | [11. Graph analysis](https://classroom.udacity.com/courses/ud758/lessons/6374209610/concepts/68428077310923)                  |                                  |                                                                          | 
| 8      | 2/28/2019  |                                                                     |                                     | [Spark GraphX](/spark/spark-graphx.html)                     |  Project Proposal Due (3/3/2019)                                       | 
| 9      | 3/5/2019  | **Guest lecture by Jim Rehg**                                           | [12. Dimensionality Reduction](https://classroom.udacity.com/courses/ud758/lessons/6334098665/concepts/63762434410923)        |                                  |                                                                          | 
| 9      | 3/7/2019  |                                                                     |                                     | [Deep Learning Lab](/dl/dl-setup.html)  |  HW4 Due (3/10/2019)                                           | 
| 10     | 3/12/2019 |                                                                | [13. Patient similairty](https://classroom.udacity.com/courses/ud758/lessons/6375269344/concepts/68571686430923)              | [Deep Learning Lab by Sungtae An - Cont.](/dl/dl-setup.html)    |                                                                          | 
| 10     | 3/14/2019 |                                                                     | [14. DNN]                                   |         |                                                          | 
| 11     | 3/19/2019 |                                                         | [15. CNN]                                    |                                  |                                                                          | 
| 11     | 3/21/2019 |                                                         | [16. RNN]                                    |                                  |HW5 Due (3/24/2019) | 
| 12     | 3/26/2019 | Project Discussion                                                  |                                     |                                  | | 
| 12     | 3/28/2019 | Project Discussion                                                  |                                     |                                  |                                                  | 
| 13     | 4/2/2019  | Computational phenotyping with tensor factorization by Kimis Perros - Cont.   |                                     |                                  |                                                                      | 
| 13     | 4/4/2019  | **Guest lecture by Greg Cooper (UPitt)**                                |                                     |                                  |    Project Draft Due (4/7/2019)                                      | 
| 14     | 4/9/2019 | **Guest**                                                               |                                     |                                  |                                                                          | 
| 14     | 4/11/2019 | **Guest lecture: S. Joshua Swamidass (Wash U.)**                        |                                     |                                  |                                                                          | 
| 15     | 4/16/2019 |                                                                |                                     |                                  |                                                                          | 
| 15     | 4/18/2019 | **Guest lecture by Walter 'Buzz' Stewart (Sutter Health)**    |                                     |                                  |                                                                          | 
| 16     | 4/23/2019 |                                                                |                                     |                                  |                                                                          | 
| 16     | 4/25/2019 |                                                                     |                                     |                                  | Final Project with code, presentation, and the final paper (4/28/2019) | 
-->


<!--
| Week # | Dates     |  Video lessons                       | Lab                              | Deliverable Due                                                          | 
|--------|-----------|---------------------------------------------------------------------|-------------------------------------|----------------------------------|--------------------------------------------------------------------------| 
| 1      | Aug 20-24  |  [1. Intro to Big Data Analytics](https://www.udacity.com/course/viewer#!/c-ud758/l-6311012028), [2. Course Overview](https://www.udacity.com/course/viewer#!/c-ud758/l-5046828066)     |  [Scala Basic](/spark/scala-basic.html)                      |                                                                          | 
| 2      | Aug 27-31 |  [3. Predictive Modeling](https://www.udacity.com/course/viewer#!/c-ud758/l-5484251492)              |   [Hadoop & HDFS Basics](/hadoop/hdfs-basic.html)                               |    HW1 Due (Sep 2)                                                                       | 
| 3      | Sep 3-7 |  [4.MapReduce](https://www.udacity.com/course/viewer#!/c-ud758/l-6298155413)& [HBase](/hadoop/hadoop-hbase.html)                  |                                  |                                                           | 
| 4      | Sep 10-14 |  [5.Classification evaluation metrics](https://www.udacity.com/course/viewer#!/c-ud758/l-5505090946), [6.Classification ensemble methods](https://www.udacity.com/course/viewer#!/c-ud758/l-5615268587) |   [Hadoop Pig](/hadoop/hadoop-pig.html) & [Hive](/hadoop/hadoop-hive.html)                               |   HW2 Due (Sep 16)                                                                        | 
| 5      | Sep 17-21  |  [7. Phenotyping](https://www.udacity.com/course/viewer#!/c-ud758/l-6363218753), [8. Clustering](https://www.udacity.com/course/viewer#!/c-ud758/l-6343118554)                      |  [Spark Basic](/spark/spark-basic.html), [Spark SQL](/spark/spark-sql.html)                                |                                                                          | 
| 6      | Sep 24-28 |  [9. Spark](https://www.udacity.com/course/viewer#!/c-ud758/l-6376189383/m-6861062716)                            |   [Spark Application](/spark/spark-application.html) & [Spark MLlib](/spark/spark-mllib.html)                               |    HW3 Due & Project Group Formation & Project Requirements Release (proposal/draft/final) (Sep 30)                                                                      | 
| 7      | Oct 1-5 |  [10. Medical ontology](https://www.udacity.com/course/viewer#!/c-ud758/l-6370309670)                 |  [NLP Lab](/nlp/solr.html)                                |                                                                          | 
| 8      | Oct 8-12 |  [11. Graph analysis](https://www.udacity.com/course/viewer#!/c-ud758/l-6374209610/m-6842807731), [12. Dimensionality Reduction](https://www.udacity.com/course/viewer#!/c-ud758/l-6334098665)                  | [Spark GraphX](/spark/spark-graphx.html)                                 |   Project Proposal Due (Oct 14)                                                                        | 
| 9      | Oct 15-19  |  [13. Patient similairty](https://www.udacity.com/course/viewer#!/c-ud758/l-6375269344/m-6857168643), [14. DNN](http://sunlab.org/teaching/cse6250/fall2018/dl/dl-fnn.html#artificial-neural-networks)        |   [Deep Learning Lab](/dl/dl-setup.html)                               |       HW4 Due (Oct 21)                                                                   | 
| 10     | Oct 22-26 |   [15. CNN](http://sunlab.org/teaching/cse6250/fall2018/dl/dl-cnn.html), [16. RNN](http://sunlab.org/teaching/cse6250/fall2018/dl/dl-rnn.html#recurrent-neural-networks-2)               | |                                                                          | 
| 11     | Oct 29 - Nov 2 |   Potential Guest Lecture                                    |                                  |                                                                           HW5 Due (Nov 4)
| 12     | Nov 5-9 |  Potential Guest Lecture                                                 |                                     |                                   | 
| 13     | Nov 12-16 |  Potential Guest Lecture                                    |                                  |                                                Project Draft Due (Nov 11)                          | 
| 14     | Nov 19-23 |  Project Discussion                                    |                                  |                                                                          | 
| 15     | Nov 26-30 |  Project Discussion                                     |                                  |                                                                          | 
| 16     | Dec 3-7 |   Final Exam Week                                    |                                  | Final Project Due (code + presentation + final paper) (Dec 9) | 
-->
## Previous Guest Lectures

See [RESOURCE](/resource.html) section.
