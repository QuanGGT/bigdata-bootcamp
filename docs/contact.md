---
layout: SpecialPage
---
# About Us

<!--[sunlab-team](images/avatar/aboutus.jpg "Sunlab team")-->

![sunlab-team](images/avatar/aboutus.jpg "Sunlab team")

## WebEx Office Hour

We will host office hour(30 mins) through WebEx or bluejean **Weekly**. Email him/her if you have trouble to join the meeting. See below table for detailed schedule. Please find the TAs' scheduele for each week on Piazza.

| Photo| Name|Time (EST)             | Location or Web Link |
| :-------------: | :-------------: | ---------------- | --------------------------------------------------------------------------------------|
|![minipic](images/avatar/Jimeng.png)   |  Jimeng Sun, instructor jsun<span style="display:none">hello</span>&nbsp;AT<span style="display:none">world</span>&nbsp;cc.gatech.edu     |      |       Request by email         |
|![minipic](images/avatar/MingLiu.jpg) | Ming Liu , PhD,  Head TA mliu302<span style="display:none">hello</span>&nbsp;AT<span style="display:none">world</span>&nbsp;gatech.edu| Request by email |<https://gatech.webex.com/meet/mliu302>
|![minipic](images/avatar/Jingyi.jpeg) | Jingyi Li , MS,  TA jli647<span style="display:none">hello</span>&nbsp;AT<span style="display:none">world</span>&nbsp;gatech.edu| See announcement on Piazza |<https://gatech.webex.com/meet/jli647>
![minipic](images/avatar/quanguo.jpg) | Quan Guo , PhD,  TA qguo48<span style="display:none">hello</span>&nbsp;AT<span style="display:none">world</span>&nbsp;gatech.edu| See announcement on Piazza |<https://gatech.webex.com/meet/qguo48>
|![minipic](images/avatar/Pengwei.jpeg) | Pengwei Li, MS, TA pli330<span style="display:none">hello</span>&nbsp;AT<span style="display:none">world</span>&nbsp;gatech.edu| See announcement on Piazza | <https://gatech.webex.com/meet/pli330>
|![minipic](images/avatar/Haomin.jpg) | Haomin Lin, MS, TA humaslin97<span style="display:none">hello</span>&nbsp;AT<span style="display:none">world</span>&nbsp;gatech.edu| See announcement on Piazza | <https://bluejeans.com/5699570405>
|![minipic](images/avatar/Qixuan.jpeg) | Qixuan(Jason) Huang, MS, TA qhuang42<span style="display:none">hello</span>&nbsp;AT<span style="display:none">world</span>&nbsp;gatech.edu| See announcement on Piazza | <https://gatech.webex.com/meet/qhuang42>
|![minipic](images/avatar/Yixu.jpg) | Yixu Yang, MS, TA yyang847<span style="display:none">hello</span>&nbsp;AT<span style="display:none">world</span>&nbsp;gatech.edu| See announcement on Piazza | <https://gatech.webex.com/meet/yyang847>
|![minipic](images/avatar/Zhenda.jpeg) | Zhenda Li, MS, TA zli493<span style="display:none">hello</span>&nbsp;AT<span style="display:none">world</span>&nbsp;gatech.edu| See announcement on Piazza | <https://gatech.webex.com/meet/zli493>




